#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/string.h>

#define PDEBUG(fmt, args...)                                                   \
    printk(KERN_DEBUG "%s:%s:%d == " fmt "\n", __FILE__, __func__, __LINE__,   \
           ##args)

static char* msg = "Hello, World!";
module_param(msg, charp, S_IRUGO);

struct cdriver
{
    char something;
    struct list_head next;
};

static struct list_head head;

MODULE_DESCRIPTION("My kernel module");
MODULE_AUTHOR("Me");
MODULE_LICENSE("GPL");

static int __init
dummy_start(void)
{
    size_t i;
    struct cdriver* nou;
    INIT_LIST_HEAD(&head);
    for (i = 0; i < strlen(msg); i++) {
        nou = kmalloc(sizeof *nou, GFP_KERNEL);
        if (nou) {
            nou->something = msg[i];
            list_add(&nou->next, &head);
            PDEBUG("Adding %c", msg[i]);
        } else {
            PDEBUG("Allocation has failed...");
        }
    }
    return 0;
}

static void __exit
dummy_exit(void)
{
    struct list_head *it1 = NULL, *it2 = NULL;
    struct cdriver* elem = NULL;
    size_t no = 0;
    printk("---------------List has ");
    list_for_each(it1, &head) { no++; }
    printk("---------------%zu elements\n", no);
    list_for_each_safe(it1, it2, &head)
    {
        elem = list_entry(it1, struct cdriver, next);
        PDEBUG("Deleting %c", elem->something);
        kfree(elem);
        list_del(it1);
    }
    no = 0;
    printk("---------------List has ");
    list_for_each(it1, &head) { no++; }
    printk("---------------%zu elements\n", no);
}

module_init(dummy_start);
module_exit(dummy_exit);
