#ifndef DBG_H
#define DBG_H

#undef PDEBUG
#ifdef NOOB_DEBUG

#ifdef __KERNEL__

#define PDEBUG(fmt, args...)                                                   \
  printk(                                                                      \
    KERN_DEBUG "%s:%s:%d == " fmt "\n", __FILE__, __func__, __LINE__, ##args)

#else /*KERNEL NOT DEFINED*/

#define PDEBUG(fmt, args...)                                                   \
  fprintf(stderr, "%s:%s:%d == " fmt "\n", __FILE__, __func__, __LINE__, ##args)

#endif /*KERNEL*/

#else /*NOOB_DEBUG NOT DEFINED*/

#define PDEBUG(fmt, args...)

#endif /*NOOB_DEBUG*/

#endif /*KERNEL*/
