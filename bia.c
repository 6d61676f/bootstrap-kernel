#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/stat.h>

static char* msg = "Bia e hateful";
static int nr = 20;

module_param(msg, charp, S_IRUGO);
module_param(nr, int, S_IRUGO);

static int
biaStart(void)
{
  printk("%s %d\n", __func__, __LINE__);
  printk("Params:%s %d\n", msg, nr);
  return 0;
}

static void
biaStop(void)
{
  printk("Pa Bia\n");
}

module_init(biaStart);
module_exit(biaStop);
