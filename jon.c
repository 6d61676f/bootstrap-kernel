#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

static dev_t jon;

static int __init
jon_init(void)
{
  if (alloc_chrdev_region(&jon, 0, 1, "jonVagabond") < 0) {
    printk("Fuk...\n");
    return -1;
  }
  printk("Major %u Minor %u\n", (unsigned)MAJOR(jon), (unsigned)MINOR(jon));
  return 0;
}

static void __exit
jon_exit(void)
{
  unregister_chrdev_region(jon, 1);
}

module_init(jon_init);
module_exit(jon_exit);
