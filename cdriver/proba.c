#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stropts.h>

#define STRINGY "Ceva\n"
#define ARRSIZE 11

// Keep in sync with cdriver.c
#define FERGIE_IOGETSIZE 2148034080
#define FERGIE_IOGETARSIZE 2148034081
#define FERGIE_IOGETELSIZE 2148034082
#define FERGIE_IORESET 26147

const int ioctls_fergie[] = { FERGIE_IOGETSIZE,
                              FERGIE_IOGETARSIZE,
                              FERGIE_IOGETELSIZE,
                              FERGIE_IORESET };

int
main(void)
{
  size_t ioctlVal = 0;
  FILE* f = fopen("/dev/fergie0", "a+");
  char ceva[256] = { 0 };
  if (f) {
    for (size_t i = 0; i < ARRSIZE; i++) {
      printf("offset before %zu\n", (long unsigned)ftell(f));
      fprintf(f, "%zu:%s", i, STRINGY);
      fflush(f);
      printf("offset after %zu\n", (long unsigned)ftell(f));
    }
  }
  fseek(f, 0, SEEK_SET);
  while (fscanf(f, "%s", ceva) > 0) {
    printf("'%s'\n", ceva);
  }

  for (int i = 0; i < sizeof ioctls_fergie / sizeof ioctls_fergie[0]; ++i) {
    if (ioctl(fileno(f), ioctls_fergie[i], &ioctlVal) != -1) {
      printf("ioctls[%d] - %zu\n", i, ioctlVal);
    } else {
      printf("ioctls[%d]:%s\n", i, strerror(errno));
    }
    ioctlVal = 0;
  }

  fclose(f);
  f = NULL; // NOTE for valgrind
  return 0;
}
