#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/uaccess.h>

#define PDEBUG(fmt, args...)                                                   \
  printk(KERN_DEBUG "%s:%d == " fmt "\n", __func__, __LINE__, ##args)

#define DEVNAME "fergie"
#define ARSIZE 10
#define ELSIZE 100
#define FERGIE_MAGIC 'f'
#define FERGIE_IOGETSIZE _IOR(FERGIE_MAGIC, 0x20, size_t)
#define FERGIE_IOGETARSIZE _IOR(FERGIE_MAGIC, 0x21, size_t)
#define FERGIE_IOGETELSIZE _IOR(FERGIE_MAGIC, 0x22, size_t)
#define FERGIE_IORESET _IO(FERGIE_MAGIC, 0x23)
#define FERGIE_MAX_NUM 0x23

MODULE_DESCRIPTION("Char Driver Shit");
MODULE_AUTHOR("sarpedon");
MODULE_LICENSE("GPL");

struct cdriver_data
{
  unsigned char** data;
  struct list_head next;
};

static struct cdriver
{
  struct mutex lock;
  struct cdev i_cdev;
  struct list_head head;
  size_t arrSize;
  size_t elemSize;
  size_t totalDeviceSize;
} * meinCD;

static int
freeCDRIVER(struct cdriver* mcd)
{
  struct cdriver_data* cdd = NULL;
  struct list_head *it1 = NULL, *it2 = NULL;
  size_t i = 0;
  size_t deb = 0;

  if (!mcd) {
    PDEBUG("Called with null argument...returning");
    return 0;
  }
  if (!list_empty(&mcd->head)) {
    list_for_each_safe(it1, it2, &mcd->head)
    {
      PDEBUG("for_each %zu", deb++);
      cdd = list_entry(it1, struct cdriver_data, next);
      PDEBUG("cdriver_data %p", cdd);
      PDEBUG("cdriver_data->data %p", cdd->data);
      if (cdd && cdd->data) {
        size_t li = 0;
        for (i = 0; i < mcd->arrSize && cdd->data[i]; i++) {
          PDEBUG("deleting list entry %zu %p", li++, cdd->data[i]);
          kfree(cdd->data[i]);
        }
        cdd->data = NULL;
        list_del(it1);
        kfree(cdd);
      }
    }
    INIT_LIST_HEAD(&mcd->head);
  }

  mcd->elemSize = ELSIZE;
  mcd->arrSize = ARSIZE;
  mcd->totalDeviceSize = 0;

  return 0;
}

long
ioctlCDRIVER(struct file* fp, unsigned int cmd, unsigned long arg)
{
  struct cdriver* cdp = fp->private_data;
  long retVal = 0;
  if (mutex_lock_interruptible(&cdp->lock)) {
    return -ERESTARTSYS;
  }
  if (_IOC_TYPE(cmd) != FERGIE_MAGIC) {
    mutex_unlock(&cdp->lock);
    return -ENOTTY;
  }
  if (_IOC_NR(cmd) > FERGIE_MAX_NUM) {
    mutex_unlock(&cdp->lock);
    return -ENOTTY;
  }
  switch (cmd) {
    case FERGIE_IOGETSIZE:
      put_user(cdp->totalDeviceSize, (size_t __user*)arg);
      break;
    case FERGIE_IOGETARSIZE:
      put_user(cdp->arrSize, (size_t __user*)arg);
      break;
    case FERGIE_IOGETELSIZE:
      put_user(cdp->elemSize, (size_t __user*)arg);
      break;
    case FERGIE_IORESET:
      freeCDRIVER(cdp);
      break;
    default:
      retVal = -ENOTTY;
  }
  mutex_unlock(&cdp->lock);
  return retVal;
}

static int
releaseCDRIVER(struct inode* inode_p, struct file* fp)
{
  // PDEBUG("Nothing to see here...called as opposite of 'open'");
  return 0;
}

static ssize_t
readCDRIVER(struct file* fp, char __user* data, size_t count, loff_t* offp)
{

  struct cdriver* cdriver = fp->private_data;
  if (mutex_lock_interruptible(&cdriver->lock)) {
    return -ERESTARTSYS;
  }
  struct cdriver_data* cdata = NULL;
  struct list_head* it = NULL;
  size_t i = 0;
  const size_t elem_size = cdriver->elemSize;
  const size_t array_size = cdriver->arrSize;
  const size_t item_size = elem_size * array_size;
  const size_t itemOffset = *offp / item_size;
  const size_t itemRest = *offp % item_size;
  const size_t elemOffset = itemRest / elem_size;
  const size_t elemRest = itemRest % elem_size;
  PDEBUG("elem_size %zu, array_size %zu, item_size %zu, itemOffset %zu, "
         "itemRest %zu, elemOffset %zu, elemRest %zu",
         elem_size,
         array_size,
         item_size,
         itemOffset,
         itemRest,
         elemOffset,
         elemRest);
  PDEBUG("count %zu, offset %zu", count, (size_t)*offp);

  if (*offp >= cdriver->totalDeviceSize) {
    mutex_unlock(&cdriver->lock);
    return 0;
  }

  if (*offp + count > cdriver->totalDeviceSize) {
    count = cdriver->totalDeviceSize - *offp;
  }

  PDEBUG("&cdriver->head %p", &cdriver->head);

  list_for_each(it, &cdriver->head)
  {
    if (i++ == itemOffset) {
      break;
    }
  }

  cdata = list_entry(it, struct cdriver_data, next);

  if (cdata == NULL || cdata->data == NULL || cdata->data[elemOffset] == NULL) {
    mutex_unlock(&cdriver->lock);
    return 0;
  }

  if (count > elem_size - elemRest) {
    count = elem_size - elemRest;
  }

  if (copy_to_user(data, cdata->data[elemOffset] + elemRest, count)) {
    mutex_unlock(&cdriver->lock);
    return -EFAULT;
  }

  *offp += count;
  mutex_unlock(&cdriver->lock);
  return count;
}

static ssize_t
writeCDRIVER(struct file* fp,
             const char __user* data,
             size_t count,
             loff_t* offp)
{
  struct cdriver* cdr = fp->private_data;
  if (mutex_lock_interruptible(&cdr->lock)) {
    return -ERESTARTSYS;
  }
  struct cdriver_data* cdata = NULL;
  struct list_head* it = NULL;
  size_t i = 0;
  const size_t elem_size = cdr->elemSize;
  const size_t array_size = cdr->arrSize;
  const size_t item_size = elem_size * array_size;
  const size_t itemOffset = *offp / item_size;
  const size_t itemRest = *offp % item_size;
  const size_t elemOffset = itemRest / elem_size;
  const size_t elemRest = itemRest % elem_size;
  PDEBUG("elem_size %zu, array_size %zu, item_size %zu, itemOffset %zu, "
         "itemRest %zu, elemOffset %zu, elemRest %zu",
         elem_size,
         array_size,
         item_size,
         itemOffset,
         itemRest,
         elemOffset,
         elemRest);
  PDEBUG("count %zu, offset %zu", count, (size_t)*offp);
  //
  //  PDEBUG("&cdriver->head %p", &cdr->head);
  PDEBUG("fp->f_pos %zu", (long unsigned)fp->f_pos);
  PDEBUG("&fp->f_pos %p\noffp %p", &fp->f_pos, offp);

  if (!list_empty(&cdr->head)) {
    // PDEBUG("List is not empty");
    list_for_each(it, &cdr->head)
    {
      if (i++ == itemOffset) {
        break;
      }
    }
    // PDEBUG("List item %zu it %p", i, it);
    cdata = list_entry(it, struct cdriver_data, next);
    // PDEBUG("Cdata %p", cdata);
  }

  if (cdata == NULL) {
    cdata = kmalloc(sizeof *cdata, GFP_KERNEL);
    PDEBUG("Creating cdriver_data %p", cdata);
    memset(cdata, 0x00, sizeof *cdata);
    list_add(&cdata->next, &cdr->head);
  }

  if (cdata->data == NULL) {
    cdata->data = kmalloc(array_size * sizeof *cdata->data, GFP_KERNEL);
    PDEBUG("Creating cdriver_data->data %p", cdata->data);
    memset(cdata->data, 0x00, array_size * sizeof *cdata->data);
  }

  if (cdata->data[elemOffset] == NULL) {
    cdata->data[elemOffset] = kmalloc(elem_size, GFP_KERNEL);
    PDEBUG("Creating cdriver_data->data[%zu] %p", elemOffset, cdata->data[i]);
    memset(cdata->data[elemOffset], 0x00, elem_size);
  }

  if (count > elem_size - elemRest) {
    count = elem_size - elemRest;
  }

  if (copy_from_user(cdata->data[elemOffset] + elemRest, data, count)) {
    mutex_unlock(&cdr->lock);
    return -EFAULT;
  }

  *offp += count;

  if (cdr->totalDeviceSize < *offp) {
    cdr->totalDeviceSize = *offp;
  }

  mutex_unlock(&cdr->lock);
  return count;
}

static loff_t
seekCDRIVER(struct file* fp, loff_t offset, int whence)
{
  struct cdriver* const mcd = fp->private_data;
  loff_t newOffset = -1;
  if (mutex_lock_interruptible(&mcd->lock)) {
    return -ERESTARTSYS;
  }
  if (whence == SEEK_SET) {
    newOffset = offset;
    PDEBUG("SEEK_SET offset=%zu\n", (long unsigned)newOffset);
  } else if (whence == SEEK_CUR) {
    PDEBUG("fp->f_pos %zi - offset %zi - mcd->totalDeviceSize %zi",
           (ssize_t)fp->f_pos,
           (ssize_t)offset,
           (ssize_t)mcd->totalDeviceSize);
    newOffset = fp->f_pos + offset;
    if (newOffset > mcd->totalDeviceSize) {
      newOffset = mcd->totalDeviceSize - 1;
    }
    PDEBUG("SEEK_CUR offset=%zu\n", (long unsigned)newOffset);
  } else if (whence == SEEK_END) {
    PDEBUG("fp->f_pos %zi - offset %zi - mcd->totalDeviceSize %zi",
           (ssize_t)fp->f_pos,
           (ssize_t)offset,
           (ssize_t)mcd->totalDeviceSize);
    newOffset = mcd->totalDeviceSize + offset;
    PDEBUG("SEEK_END offset=%zu\n", (long unsigned)newOffset);
  }

  fp->f_pos = newOffset;

  mutex_unlock(&mcd->lock);
  return newOffset;
}

static int
openCDRIVER(struct inode* inode_p, struct file* fp)
{
  struct cdriver* mcd = container_of(inode_p->i_cdev, struct cdriver, i_cdev);
  if (mcd) {
    // PDEBUG("Got my cdriver structure from inode...%p", mcd);
  } else {
    return -1;
  }
  if (mutex_lock_interruptible(&mcd->lock)) {
    return -ERESTARTSYS;
  }
  fp->private_data = mcd;
  /// NOTE
  /// https://www.gnu.org/software/libc/manual/html_node/Access-Modes.html#Access-Modes
  // XXX Why does it get opened in writeonly on append? XXX
  if ((fp->f_flags & O_ACCMODE) == O_WRONLY) {
    PDEBUG("Device opened write-only...");
    freeCDRIVER(mcd);
  }
  mutex_unlock(&mcd->lock);
  return 0;
}

static struct file_operations my_fops = { .read = readCDRIVER,
                                          .write = writeCDRIVER,
                                          .release = releaseCDRIVER,
                                          .open = openCDRIVER,
                                          .llseek = seekCDRIVER,
                                          .unlocked_ioctl = ioctlCDRIVER };

static struct cdriver*
initDevice(unsigned int firstminor, unsigned int count, char* name)
{
  struct cdriver* nou = NULL;
  dev_t devno = 0;

  if (alloc_chrdev_region(&devno, firstminor, count, name)) {
    goto fail;
  } else {
    // PDEBUG("Created %s c %d %d succesfully", name, MAJOR(devno),
    // MINOR(devno));
    PDEBUG("sudo mknod /dev/%s0 c %d %d ", name, MAJOR(devno), MINOR(devno));
  }

  nou = kmalloc(sizeof *nou, GFP_KERNEL);
  if (!nou) {
    PDEBUG("Allocation has failed");
    goto fail;
  }

  // XXX We must add file_operations otherwise it's useless
  // struct file_operations:include/linux/fs.h:1664
  // XXX Implement read, write, open, release at the very least
  memset(nou, 0x00, sizeof *nou);
  INIT_LIST_HEAD(&nou->head);
  nou->i_cdev.owner = THIS_MODULE;
  nou->elemSize = ELSIZE;
  nou->arrSize = ARSIZE;
  // XXX init_MUTEX is gone in newer kernels
  // See linux/mutex.h
  mutex_init(&nou->lock);
  cdev_init(&nou->i_cdev, &my_fops);

  if (cdev_add(&nou->i_cdev, devno, 1)) {
    PDEBUG("cdev_add has failed...");
    goto fail;
  }

  return nou;

fail:
  PDEBUG("Failing..");
  if (nou) {
    cdev_del(&nou->i_cdev);
    kfree(nou);
  }
  unregister_chrdev_region(devno, count);
  return NULL;
}

static int
delDevice(struct cdriver* drv, unsigned int count)
{
  dev_t devno = 0;
  if (!drv) {
    PDEBUG("drv is NULL");
    return -1;
  }

  devno = drv->i_cdev.dev;

  PDEBUG("Unregistering...");
  cdev_del(&drv->i_cdev);
  freeCDRIVER(drv);
  kfree(drv);
  unregister_chrdev_region(devno, count);
  return 0;
}

static int __init
startish(void)
{
  meinCD = initDevice(0, 1, DEVNAME);
  if (meinCD) {
    PDEBUG("Initialized device");
    PDEBUG("IOCTLS are \nFERGIE_IOGETSIZE %lu\nFERGIE_IOGETARSIZE "
           "%lu\nFERGIE_IOGETELSIZE %lu\n FERGIE_IORESET %lu",
           (long unsigned)FERGIE_IOGETSIZE,
           (long unsigned)FERGIE_IOGETARSIZE,
           (long unsigned)FERGIE_IOGETELSIZE,
           (long unsigned)FERGIE_IORESET);
  } else {
    return -1;
  }
  return 0;
}

static void __exit
exitish(void)
{
  delDevice(meinCD, 1);
}

module_init(startish);
module_exit(exitish);
