#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

MODULE_DESCRIPTION("My kernel module");
MODULE_AUTHOR("Me");
MODULE_LICENSE("GPL");

static int
dummy_init(void)
{
  printk("Inserting luv\n");
  return 0;
}

static void
dummy_exit(void)
{
  printk("Removing luv\n");
}

module_init(dummy_init);
module_exit(dummy_exit);
